# secret-project

### Technical Interview Task Overview
* Find defects on the [working system](https://qa-sa06-cooking-1.jarvis.syberry.net/)
* Find and report all defects you've found

[Requirements for the system](https://1krpi7.axshare.com/#id=4wtf2y&p=1_cooking_portal_onboarding&g=1) <br>

login/password<br>
zxc@zxc.zxc / qwerty123Q!<br>

Report defects according to our [Defect Creation guide](https://gitlab.com/SyberryAcademy/syberry-academy-e07-qa-test-task/-/blob/master/Defect%20creation%20guide_SA.pdf) <br>
Reporting language – English

Deadline: Jun 16 2021, 15:40 Minsk Time

### Detailed Task Decription
1. Create a board in https://trello.com/
2. Name it Name-Surname-Technical-Interview
3. Create 2 columns: Functional Testing and Non-Functional Testing
4. Describe the defect in English following our [Defect Creation guide](https://gitlab.com/SyberryAcademy/syberry-academy-e07-qa-test-task/-/blob/master/Defect%20creation%20guide_SA.pdf)
5. In the comments to each defect report the time you've spent on it
6. Send us an email with an invite to the board <br>
Send a link to your trello board with the solved task to academy@syberry.com. Email subject: "Syberry Academy QA Technical Interview Name Surname". For example, "Syberry Academy QA Technical Interview Jane Doe".<br>
We have an authomatic filter on our mailbox. If you won't write a subject as we asked, we won't see your task and disqualify your solution. <br>
We won't check the task sent after the deadline

*Sanity Check* <br>
Please double check: <br>
your email has the proper subject <br>
each defect description follows our guide and has a time entry <br>

# What's next
If you did well on our task, our Recruiter will call you and invite you to an HR interview. If not, we'll send you an email with your results and comments.

We will need five working days to process your results.
